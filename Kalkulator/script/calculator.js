let wynik;
let pobierane = '';
let znak;


function pobierzLiczbe(c) {
  if (c == '.') {
    if (pobierane.includes(".") == false && pobierane == '') {
      pobierane += '0.'
    } else if (pobierane.includes(".") == false) {
      pobierane = pobierane.toString() + c.toString()
      document.getElementById("wyswietlacz").innerHTML = pobierane
    }
  } else {
    if (c == 10) {
      c = "00"
    }
    if (c == 0 || c == "00") {
      if (pobierane != "") {
        pobierane = pobierane.toString() + c.toString()
        document.getElementById("wyswietlacz").innerHTML = pobierane
      }
    } else {
      pobierane = pobierane.toString() + c.toString()
      document.getElementById("wyswietlacz").innerHTML = pobierane
    }
  }
}

function usuwanieAll() {
  wynik = ''
  pobierane = '';
  znak = ''
  document.getElementById("wyswietlacz").innerHTML = '0'
}

function jeden(lista) {
  var i
  var x = ''
  for (i = 0; i < lista.length - 1; i++) {
    x += lista[i]
  }
  return x
}

function zero(lista) {
  var i
  var x = ''
  for (i = 1; i < lista.length; i++) {
    x += lista[i]
  }
  return x
}

function usun() {
  if (pobierane[0] == '-' && pobierane.length == 2) {
    pobierane = ''
    document.getElementById("wyswietlacz").innerHTML = '0'
  } else if (pobierane[pobierane.length - 2] == '.' && pobierane[pobierane.length - 3] == '0' && pobierane[pobierane.length - 4] == '-') {
    pobierane = ''
    document.getElementById("wyswietlacz").innerHTML = '0'
  } else if (pobierane[pobierane.length - 2] == '.' && pobierane[pobierane.length - 3] == '0') {
    pobierane = ''
    document.getElementById("wyswietlacz").innerHTML = '0'
  } else if (pobierane[pobierane.length - 2] == '.') {
    pobierane = jeden(pobierane)
    pobierane = jeden(pobierane)
    document.getElementById("wyswietlacz").innerHTML = pobierane
  } else {
    if (pobierane.length == 1) {
      pobierane = ''
      document.getElementById("wyswietlacz").innerHTML = '0'
    } else if (pobierane != '') {
      pobierane = jeden(pobierane)
      document.getElementById("wyswietlacz").innerHTML = pobierane
    }
  }
}

function plusminus() {
	if (pobierane.length > 0){
	  if (pobierane[0] == '-') {
		pobierane = zero(pobierane)
		document.getElementById("wyswietlacz").innerHTML = pobierane
	  } else {
		pobierane = '-' + pobierane
		document.getElementById("wyswietlacz").innerHTML = pobierane
	  }
	}
}

function pobierzZnak(d) {
	if (pobierane != '' && (wynik)){
		if (znak == "-"){
			wynik -= parseFloat(pobierane)
			pobierane = ''
			document.getElementById("wyswietlacz").innerHTML = wynik
		}else if (znak == "+"){
			wynik += parseFloat(pobierane)
			pobierane = ''
			document.getElementById("wyswietlacz").innerHTML = wynik
		}else if (znak == "/"){
			wynik /= parseFloat(pobierane)
			pobierane = ''
			document.getElementById("wyswietlacz").innerHTML = wynik
		}else if (znak == "*"){
			wynik *= parseFloat(pobierane)
			pobierane = ''
			document.getElementById("wyswietlacz").innerHTML = wynik
		}else if (znak == "=") {
      wynik = parseFloat(pobierane)
      pobierane = ''
      document.getElementById("wyswietlacz").innerHTML = wynik
    }
	}else if (pobierane != ''){
		wynik = parseFloat(pobierane)
		pobierane = ''
	}
	znak = d
}
